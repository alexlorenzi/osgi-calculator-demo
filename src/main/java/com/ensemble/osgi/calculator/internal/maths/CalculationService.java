package com.ensemble.osgi.calculator.internal.maths;

import com.ensemble.osgi.calculator.Operation;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Stores equation in a Reverse Polish Notation format, and calculates this way
 */
public class CalculationService {

	private final List<CalculationToken<?>> equation;
	private final Stack<OperationToken> stack;

	public CalculationService() {
		this.equation = new LinkedList<>();
		this.stack = new Stack<>();
	}

	public void addNumber(Integer value) {
		equation.add(new NumberToken(value));
	}

	public void addOperation(Operation operation) {
		// Add to operations stack in precedence order
		while (!stack.isEmpty() && stack.peek().getValue().getPriority() >= operation.getPriority()) {
			equation.add(stack.pop());
		}
		stack.push(new OperationToken(operation));
	}

	public int evalulate() {
		// Add any remaining operations to the equation list
		while (!stack.isEmpty())
			equation.add(stack.pop());

		// Calculate
		Stack<Integer> valueStack = new Stack<>();
		for (CalculationToken<?> token : equation) {
			if (token instanceof NumberToken) {
				valueStack.push(((NumberToken) token).getValue());
			} else {
				// Process Operation (this assumes they're always binary)
				int value1 = valueStack.pop();
				int value2 = valueStack.pop();
				int result = ((OperationToken) token).operate(value1, value2);
				valueStack.push(result);
			}
		}

		return valueStack.pop();
	}

	public void clear() {
		equation.clear();
		stack.clear();
	}

	private interface CalculationToken<T> {
		T getValue();
	}

	private class NumberToken implements CalculationToken<Integer> {
		private final int value;

		private NumberToken(int value) {
			this.value = value;
		}

		@Override
		public Integer getValue() {
			return value;
		}
	}

	private class OperationToken implements CalculationToken<Operation> {
		private final Operation value;

		private OperationToken(Operation value) {
			this.value = value;
		}

		@Override
		public Operation getValue() {
			return value;
		}

		public int operate(int first, int second) {
			return this.value.evaluation(first, second);
		}
	}
}
