package com.ensemble.osgi.calculator.internal.gui;

import com.ensemble.osgi.calculator.internal.maths.CalculationService;
import com.ensemble.osgi.calculator.Operation;
import org.osgi.service.component.annotations.*;
import org.osgi.service.component.annotations.Component;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
public class Calculator extends JFrame {

	private final JTextField display;
	private final JPanel numberPad;
	private final JPanel operationPad;

	private final Map<Operation, JButton> operationButtonMap;
	private final CalculationService cruncher;

	public Calculator() {
		super("OSGi Calculator");
		setSize(300, 400);

		display = new JTextField();
		display.setEditable(false);
		display.setHorizontalAlignment(JTextField.RIGHT);

		numberPad = new JPanel(new GridLayout(4, 3));
		operationPad = new JPanel(new GridLayout(4, 1));

		Arrays.asList(7, 8, 9, 4, 5, 6, 1, 2, 3, 0).forEach(this::addNumberButton);
		addEqualsButton();
		addClearButton();

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(display, BorderLayout.NORTH);
		getContentPane().add(numberPad, BorderLayout.WEST);
		getContentPane().add(operationPad, BorderLayout.CENTER);

		operationButtonMap = new HashMap<>();
		cruncher = new CalculationService();
		clearMemory();
	}

	private void clearScreen() {
		display.setText("");
	}

	private void clearMemory() {
		cruncher.clear();
	}

	private int displayValue() {
		return "".equals(display.getText()) ? 0 : Integer.valueOf(display.getText());
	}

	private void addNumberButton(final int value) {
		JButton button = new JButton(String.valueOf(value));
		button.addActionListener(e -> display.setText(display.getText().concat(String.valueOf(value))));
		numberPad.add(button);
	}

	private void addClearButton() {
		JButton button = new JButton("C");
		button.addActionListener(e -> {
			clearScreen();
			clearMemory();
		});
		numberPad.add(button);
	}

	private void addEqualsButton() {
		JButton button = new JButton("=");
		button.addActionListener(e -> {
			// Add last values to equation list
			cruncher.addNumber(displayValue());
			int result = cruncher.evalulate();
			display.setText(String.valueOf(result));
		});
		numberPad.add(button);
	}

	private void addOperationButton(Operation operation) {
		JButton button = new JButton(operation.getIcon());
		button.addActionListener(e -> {
			cruncher.addNumber(displayValue());
			cruncher.addOperation(operation);
			clearScreen();
		});
		operationPad.add(button);
		operationPad.revalidate();
		operationButtonMap.put(operation, button);
	}

	private void removeOperationButton(Operation operation) {
		if (operationButtonMap.containsKey(operation)) {
			JButton button = operationButtonMap.get(operation);
			operationPad.remove(button);
			operationButtonMap.remove(operation);
			operationPad.revalidate();
		}
	}

	@Reference(
			cardinality = ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC,
			unbind = "unregisterOperation"
	)
	public void registerOperation(Operation operation) {
		EventQueue.invokeLater(() -> addOperationButton(operation));
	}

	public void unregisterOperation(Operation operation) {
		EventQueue.invokeLater(() -> removeOperationButton(operation));
	}

	@Activate
	public void activate() {
		EventQueue.invokeLater(() -> this.setVisible(true));
	}

	@Deactivate
	public void deactivate() {
		EventQueue.invokeLater(() -> this.dispose());
	}
}
