package com.ensemble.osgi.calculator.internal.operations;

import com.ensemble.osgi.calculator.Operation;
import org.osgi.service.component.annotations.Component;

public final class OperationHost {

	private OperationHost() {
	}

	@Component(service = Operation.class, immediate = true)
	public static class Addition implements Operation {
		@Override
		public String getIcon() {
			return "\u002B";
		}

		@Override
		public int getPriority() {
			return 0;
		}

		@Override
		public int evaluation(int a, int b) {
			return b + a;
		}
	}

	@Component(service = Operation.class, immediate = true)
	public static class Subtraction implements Operation {

		@Override
		public String getIcon() {
			return "\u2212";
		}

		@Override
		public int getPriority() {
			return 1;
		}

		@Override
		public int evaluation(int a, int b) {
			return b - a;
		}
	}

	@Component(service = Operation.class, immediate = true)
	public static class Multiplication implements Operation {

		@Override
		public String getIcon() {
			return "\u2715";
		}

		@Override
		public int getPriority() {
			return 2;
		}

		@Override
		public int evaluation(int a, int b) {
			return b * a;
		}
	}

	@Component(service = Operation.class, immediate = true)
	public static class Division implements Operation {

		@Override
		public String getIcon() {
			return "\u00F7";
		}

		@Override
		public int getPriority() {
			return 3;
		}

		@Override
		public int evaluation(int a, int b) {
			return b / a;
		}
	}

}
