package com.ensemble.osgi.calculator;

public interface Operation {

	String getIcon();

	int getPriority();

	int evaluation(int a, int b);

}
