# README #

OSGi Calculator Exercise

### What is this repository for? ###

Convert this (basic) Java Swing based calculator into an OSGi bundle that uses services to replace the current operations code. You should be able to add/remove operations dynamically and have the UI and functionality of the calculator update in response.

### How do I get set up? ###

From the commnad line run
`mvn clean install`

Then from within your karaf (or other container) instance run
`install -s mvn:com.ensemble.osgi/osgi-calculator-demo/1.0-SNAPSHOT`

### Who do I talk to? ###

* Alex Lorenzi (alex.lorenzi@ensemble.com)